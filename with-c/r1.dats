(* sockets *)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"  

staload
UN = "prelude/SATS/unsafe.sats"

%{^
#include <sys/socket.h>
#include <netinet/in.h>
%}

typedef uint16_t = $extype"uint16_t"
stadef uint16_t = [i:nat | i <= 65535] int (i)

typedef uint32_t = $extype"uint32_t"
stadef uint32_t = [i:nat | i <= 4294967295] int (i)

typedef ushort = $extype"unsigned short"
stadef ushort = uint16_t

macdef SOCK_STREAM = $extval(int , "SOCK_STREAM") 
macdef PF_INET  = $extval(int,"PF_INET")
macdef AF_INET  = $extval(int,"AF_INET")
macdef INADDR_ANY = $extval(uint32_t,"INADDR_ANY")


typedef sockaddr = $extype_struct "struct sockaddr" of 
{sa_family = ushort , sa_data = @[char][14]}

typedef in_addr = $extype_struct "struct in_addr"  of {s_addr = uint32_t} 
 

typedef sockaddr_in = $extype_struct "struct sockaddr_in" of 
{sin_family = uint16_t , sin_port = uint16_t , 
  sin_addr=in_addr , sin_zero = @[char][8]}

extern
fun socket 
(domain : int , type : int , protocol : int) : int = "ext#socket"

(* how to use 'ext#bind' instead of 'mac#' *)
extern
fun bind
 (sockid : int , addr : &sockaddr_in?  , size : size_t) : int 
 = "mac#bind"
  

extern
fun htons (hostshort : uint16_t) : uint16_t = "mac#htons"

extern
fun htonl (histlong : uint32_t) : uint32_t = "mac#htonl"

var s : sockaddr_in 

val () = (s.sin_family := AF_INET ;
          s.sin_port := htons (8001) ;
          s.sin_addr.s_addr := htonl (INADDR_ANY) ;)          
          
val skt = socket (PF_INET , SOCK_STREAM , 0)          

val st = bind (skt , s , sizeof<sockaddr_in>) 


extern
fun close (sid : int) : int = "mac#"
val cst = close (skt)


implement main0 () = {
  val () = println! ("sockets ...");
}
  


(*
val a = sizeof<sockaddr>
val b = sizeof<in_addr>
val c = sizeof<sockaddr_in>


val () = println! (a)
val () = println! (b)
val () = println! (c)
var sk_addr : sockaddr

val () = sk_addr.sa_family := 2
val () = println! (sk_addr.sa_family)
*)

