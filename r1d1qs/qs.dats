#include "share/atspre_define.hats"
#include "share/atspre_staload.hats" 
staload UN = "prelude/SATS/unsafe.sats"

(*  

This code tries to rep quicksort as shown
here "https://www.cs.cmu.edu/~rjsimmon/15122-s13/08-qsort.pdf"
Main Purpose of this code is to try to convert some 
contracts given above into ATS-2 and nothing more , so this
code might not be a quicksort or even produce result which is
not sorted . This is only for learning purpose.

All contracts are not converted , because that will take some 
more effort and I don't know ATS that much , will try cover them
as I move along

*)

val () = println! ("quicksort on a linear array")

extern
fun sort
{n:nat}{lower:nat}{upper:nat | lower <= upper  && upper <= n}
(A : &(@[int][n]), n : int (n), lw : int (lower) , up : int (upper)) : void

extern
fun partition
{n:nat} {lower,pivot_index,upper : nat | 
lower <= upper && lower <= pivot_index && pivot_index <upper
&& upper <= n} 
(A : &(@[int][n]) , n : int (n) , lower : int (lower) ,
 pivot_index : int (pivot_index) ,  upper : int (upper) ) :
 [m : nat | lower <= m && m < upper ] int (m)


//This is a while loop in partition 
extern
fun loop
{n,lower,upper:nat | lower <= upper && upper <= n}
{left:nat | lower <= left } 
{right : int | left <= right+1 && (right+1) < upper  }
(A : &(@[int][n]) , pivot : int ,left : int (left) , right : int (right)) : 
[l:nat | lower <= l && l < upper] [r:int |  l == r+1  ] (int (l),int (r))

extern
fun swap{i,j,n:nat | i < n && j < n} 
(A : &(@[int][n]) , i : int (i) , j : int (j)) : void

(* implementations *)

implement
swap (A , i  , j ) = array_interchange (A,i2sz(i),i2sz(j))



implement
loop{n,lower,upper}{left}{right}(A,pivot,left,right) =      

 if (left <= right)
 then if (A[left] <= pivot) 
      then
        loop{n,lower,upper}{left+1}{right}(A,pivot,left+1,right)         
      else let 
            val () = swap (A,left,right)
           in
            loop{n,lower,upper}{left}{right-1}(A,pivot,left,right-1) 
           end           
  else (left,right)



implement
partition{n}{lower,pivot_index,upper} (A,n,lower,pp_index,upper)
= let  
  val pivot = A[pp_index]
  val () = swap (A , pp_index , upper-1)
  val (l,r) =loop{n,lower,upper}{lower}{upper-2} (A,pivot,lower,upper-2)   
  val () = swap (A , l , upper-1)  
  in
    l
  end


implement
sort{n}{lower}{upper} (A,n,l,u) = 
  if ((u-l) <= 1)
  then ()
  else let  
  val pp = l + g1int_ndiv((u-l),2)
  val mid = partition{n}{lower,lower+(upper-lower)/2,upper} (A,n,l,pp,u)
  val () = sort{n}{lower}{..} (A,n,l,mid) 
  val () = sort{n}{..}{upper} (A,n,mid+1,u)
  in end




fun print_arr2{n:nat} (A : &(@[int][n]) ,  n : int (n)) : void = let
fun loop{i,n :nat | i <= n} 
(A : &(@[int][n]) , n : int (n) , i : int (i)) : void = 
if (i < n) 
then let 
val x = array_get_at_gint<int> (A,i) in
(print(x) ; print(",") ; loop (A,n,i+1)) end
else ()
in
  println!("");
  loop (A,n,0);
  println!("");
end

  
implement main0 () = {
  var A = @[int](55,59,58,69,30,79,3,25,93,48)
  val () = print_arr2(A,10)  
  val () = sort (A,10,0,10)
  val () = print_arr2(A,10)
  
}




