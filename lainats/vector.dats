(* Vectors *)

#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
staload UN = "prelude/SATS/unsafe.sats"
(* staload "vector.sats" *)

(* Purpose of this code is to ask the question on ats mailing list 
   and get a quick review of code style *)


absvt@ype vector_vtype (a:t@ype , n : int) = ptr 
vtypedef vector (a : t0p , n : int ) = vector_vtype (a,n)

extern
fun{a:t@ype}
make_vector_n{n:nat} (psz : arrpsz(a,n)) : vector (a , n)

(* For un-initialized vector of length n *)
extern
fun{a:t@ype}
make_uninit_vector_n{n:nat} (int (n)) : vector (a? , n)

extern
fun{a:t@ype}
delete_vector{n:nat} (vec : vector (a , n) ) : void


extern
fun{a:t@ype}
vector_length {n:nat} (vec : !vector (a,n)) : int (n)


extern
fun{a:t@ype}
vector_get_i{i,n:nat | i < n} (!vector (a , n) , int (i)) : a


extern
fun{a:t@ype}
vector_set_i{i,n:nat | i < n} (!vector (a , n), int (i) , a) : void

(* just a utility for dumping vector *)
extern
fun{a:t@ype}
print_vector{n:nat} (vec : !vector (a , n)) : void


(*** Vector Operation ***)

(* This is non destructive addition *)
extern
fun{a:t@ype}
vector_add{n:nat } 
(v1 : !vector(a,n) , v2 : !vector(a,n)) : vector (a , n)

extern
fun{a:t@ype}
elem_add (x1:a,x2:a) : a


(************************************************************************)

(* IMPLEMENTATION *)

(* Note : Here in this impl  vector is a tuple , whose 1st comp is array 
          and 2nd is it's size 
*)
                    
assume vector_vtype (a:t0p , n:int) = (arrayptr (a,n),int (n))

implement{a}
make_vector_n{n} (psz) = let
  var n : size_t
  val res = arrpsz_get_ptrsize (psz , n)
  in (res,sz2i(n)) end


implement{a}
make_uninit_vector_n (n) = 
  (arrayptr_make_uninitized<a>(i2sz(n)) , n)

implement{a}
delete_vector(vec) = arrayptr_free{a} (vec.0)


implement{a}
vector_length{n} (v) = v.1


implement{a}
vector_get_i (v, i) = 
  arrayptr_get_at_gint (v.0 , i)
  

implement{a}
vector_set_i (v, i,vval) = 
  arrayptr_set_at_gint (v.0 , i , vval)


implement{a}
print_vector{n} (v) = let  
  val out = stdout_ref
  val () = fprint(out,v.0,i2sz(v.1))
  in () end

  
implement{a}
vector_add{n}(v1,v2) = let
val n = vector_length (v1)
val v3 = make_uninit_vector_n<a> (n)

implement
  array_initize$init<a> (i,x) =  let
    val i = $UN.cast{sizeLt(n)}(i)
    val (fpf1 | v1) = decode($vcopyenv_vt (v1))   
    val (fpf2 | v2) = decode($vcopyenv_vt (v2))
    val ()  = x := elem_add<a> (vector_get_i<a> (v1,sz2i(i)),
                             vector_get_i<a> (v2,sz2i(i)))    
    prval () = fpf1(v1)
    prval () = fpf2(v2) in end
       
val () = arrayptr_initize<a>(v3.0,i2sz(n))

in v3 end



// add two vector and print
implement main0 () = {
  val v1 =  make_vector_n<int> ($arrpsz{int}(1,2,3,4))
  val v2 =  make_vector_n<int> ($arrpsz{int}(100,200,300,400))
  val () = (print_vector<int> (v1) ; println! () ;
            print_vector<int> (v2) ; println! ())
  implement
  elem_add<int> (x1 , x2 ) = x1 + x2

  val vres = vector_add<int> (v1,v2) 
  val () = (print_vector<int> (vres) ; println! ()) 
  val () = (delete_vector(v1) ; delete_vector(v2) ; delete_vector(vres))
  }
